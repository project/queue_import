Queue Import

This module is meant to give another option to migrating content into Drupal 8/9 sites. It uses the Queue API and Drush commands to queue content and then use QueueWorkers to use that queued data to create entities on the new site.

Currently, the module can be used to see an example of queueing and importing content by enabling the module and running the drush command of "drush itest" to import a single article into the site.

The piece that's currently in progress is a way to plug in a database connection to a legacy Drupal 7 database and queue content directly out of that. The trick is creating the QueueWorkers with the correct field. At this time, I'm working on the automation of the creation of those workers.

Essentially, we pull the fields from the D7 db and create a QueueWorker with those fields mapped out along with a list of currently available fields from the D8/D9 site. That, along with the methods for converting that queued data into taxonomy terms or media entities, will allow you to begin importing content quickly and in a way that will you allow you to update the content on any follow-up imports based on mapping the legacy Node ID.

I'll continue to update the information here as we get farther along.

Instructions after installing the module:

 - Run "drush itest"
 - Run "drush queue-run node_queue_processor -v"
 - Navigate to the form at admin/config/development/queue-import
 - Fill in the information for a database connection (must be Drupal 7)
 - Run "drush d7fm article" (article will create MapArticleQueueProcessor.php, page would create MapPageQueueProcessor.php, etc.)
 - Fix the mapping in MapArticleQueueProcessor.php and rename to ArticleQueueProcessor.php
 - Run "drush d7 article 1" (content_type and query limit for how many to queue)
 - Run "drush queue-run article_queue_processor"
 - Check admin/content for new content and review errors
 - If needed, you can remove all imported content with "drush qicount article --delete"
