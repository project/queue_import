<?php

namespace Drupal\queue_import\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\menu_link_content\Entity\MenuLinkContent;
use Drupal\node\Entity\Node;
use Drupal\path_alias\Entity\PathAlias;
use Drupal\taxonomy\Entity\Term;
use Drupal\media\Entity\Media;
use Drupal\Core\File\FileSystemInterface;

/**
 * Class ImportContentController.
 */
class QueueImportController extends ControllerBase {

  /**
   * {@inheritdoc }
   */
  public static function cleanLines(string $gitbooksummaryline) {
    $clean_line = trim(str_replace('* ', '', $gitbooksummaryline));
    $clean_line = trim(str_replace('/README', '', $clean_line));
    preg_match('/\[(.*?)\]/s', $clean_line, $matched_title);
    preg_match('/\]\((.*?)$/s', $clean_line, $matched_alias);
    $matched_title = substr($matched_title[0], 1, -1);
    $matched_alias = substr($matched_alias[0], 2, -4);

    if (!str_starts_with($matched_alias, '/')) {
      $matched_alias = '/' . $matched_alias;
    }

    return ['matched_title' => $matched_title, 'matched_alias' => $matched_alias];
  }

  /**
   * {@inheritdoc}
   */
  public static function generateMediaEntity($uri) {
    $fid = NULL;
    $moduleHandler = \Drupal::service('module_handler');
    if ($moduleHandler->moduleExists('media')) {
      $file = file_get_contents($uri);
      $filename = basename($uri);
      $file = file_save_data($file, "public://$filename", FileSystemInterface::EXISTS_REPLACE);
      if ($file) {
        $fid = $file->id();

        // Create an image from a url.
        $fields = [
          'bundle' => 'image',
          'name' => $uri,
          'field_media_image' => [
            'target_id' => $fid,
            'alt' => '',
            'title' => '',
          ],
        ];
        $fields = array_filter($fields);
        $media = Media::create($fields);
        $media->save();
      }
    }
    else {
      \Drupal::logger('queue_import')->error('Media module not enabled.');
    }

    return $fid;
  }

  /**
   * {@inheritdoc}
   */
  public static function setToTerm($name) {
    // Create a term from imported data.
    $term = Term::create([
      'vid' => 'tags',
      'name' => $name,
    ]);
    $term->save();

    return $term->id();
  }

  /**
   * {@inheritdoc}
   */
  public static function queueWorkerHeader($content_type) {
    $oct = $content_type;
    $processor = $content_type . "_queue_processor";
    $content_type = ucfirst($content_type);
    $qw = $content_type . "QueueProcessor";
    $header = "<?php

namespace Drupal\queue_import\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\\node\Entity\Node;
use Drupal\queue_import\Controller\QueueImportController;

/**
 * Processes $content_type Imports.
 *
 * @QueueWorker(
 *   id = \"$processor\",
 *   title = @Translation(\"Task Worker: $content_type Queue Processor\"),
 *   cron = {\"time\" = 60}
 * )
 */
class $qw extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem(\$data) {
    \$node = QueueImportController::outputNode(\$data['nid']);

    if (!is_object(\$node)) {
      \$fields = [
        'type' => '$oct',
      ];
    \t\$node = Node::create(\$fields);
    }\n\r";

    return $header;
  }
  public static function queueWorkerFooter() {
    $footer = "
  }

}";
    return $footer;
  }

  /**
   * {@inheritdoc}
   */
  public static function outputTerms($terms) {
    $output_terms = [];
    foreach ($terms as $term) {
      $output_term = QueueImportController::setToTerm($term);
      $output_terms[] = $output_term;
    }

    return $output_terms;
  }

  /**
   * {@inheritdoc}
   */
  public static function outputNode($nid) {
    $query = \Drupal::entityQuery('node')->condition('field_migration_id', $nid);
    $nids = $query->execute();
    $nid = reset($nids);
    if (!empty($nid) && is_numeric($nid)) {
      // Return existing node.
      $node = Node::load($nid);
    }
    else {
      $node = '';
    }

    return $node;
  }

  /**
   * Manipulate HTML as appropriate.
   *
   * Here, we're using it to replace legacy images with Media-based ones.
   */
  public static function processHtml($html) {
    $dom = new \DOMDocument();
    libxml_use_internal_errors(TRUE);
    $dom->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
    libxml_use_internal_errors(FALSE);
    $imgs = $dom->getElementsByTagName("img");
    $i = $imgs->length - 1;
    while ($i > -1) {
      /** @var \DOMElement $img */
      $img = $imgs->item($i);
      $src = $img->getAttribute('src');
      $embed_caption = $img->parentNode->nodeValue;
      $mediaAsset = self::generateMediaEntity(basename($src), $embed_caption);

      if (is_object($mediaAsset)) {
        if (property_exists($mediaAsset, 'field_image_caption')) {
          $caption = $mediaAsset->get('field_image_caption')->value;
        }
        else {
          $caption = NULL;
        }

        $media = $dom->createElement('drupal-entity');
        $media->setAttribute('data-align', 'none');
        $media->setAttribute('data-caption', $caption);
        $media->setAttribute('data-embed-button', 'embed_image');
        $media->setAttribute('data-entity-embed-display', 'view_mode:media.embedded');
        $media->setAttribute('data-entity-type', 'media');
        $media->setAttribute('data-entity-uuid', $mediaAsset->uuid());
        $img->parentNode->replaceChild($media, $img);
      }

      $i--;
    }

    return $dom->saveHTML();
  }

}
