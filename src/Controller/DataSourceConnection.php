<?php

namespace Drupal\queue_import\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class DataSourceConnection.
 */
class DataSourceConnection extends ControllerBase {

  /**
   * This should build out a queue worker based on a drush command.
   */
  public static function queueWorkerGenerator($content_type = 'article') {
    $connection = \Drupal::database();
    $fields = $connection->query("SELECT DISTINCT TABLE_NAME FROM information_schema.tables where TABLE_NAME like 'node__field%' OR TABLE_NAME")->fetchAll();
    if ($fields) {
      $output = QueueImportController::queueWorkerHeader($content_type);

      $output .= "\t\t\$body_image = strpos(\$data['body'], '<img');\n";
      $output .= "\t\tif (\$body_image !== FALSE) {\n";
      $output .= "\t\t\t\$body = QueueImportController::processHtml(\$data['body']);\n";
      $output .= "\t\t } else {\n";
      $output .= "\t\t\t\$body = \$data['body'];\n";
      $output .= "\t\t}\n";
      $output .= "\t\t\$node->title = \$data['title'];\n";
      $output .= "\t\t\$node->body->value = \$body;\n";
      $output .= "\t\t\$node->body->format = 'full_html';\n";
      $output .= "\t\t\$node->field_migration_id = \$data['nid'];\n";
      foreach ($fields as $field) {
        $field->TABLE_NAME = str_replace('node__', '', $field->TABLE_NAME);
        $output .= "\t\t\$node->" . $field->TABLE_NAME . " = end(\$data['replace_with_legacy_fields']);\n";
      }
      $output .= "\t\t\$node->save();";

      $legacy_fields = DataSourceConnection::fieldList();
      $output .= "\n\r// This Queue Worker is not complete--rename (eg, map123.php to 123.php) or it will be overwritten. You will need to fill out the above \$node->field values with field values from your legacy database (found below).";
      $output .= "\n\r\t\$legacy_fields = [" . $legacy_fields . "];";
      $output .= QueueImportController::queueWorkerFooter();
    }

    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public static function fieldList($list = FALSE) {
    $output = "No fields returned from query.";
    $fields = DataSourceConnection::DataSourceQuery("select field_name from field_config order by field_name asc")->fetch_all();
    if ($fields) {
      if ($list == FALSE) {
        $output = "\n";
        foreach ($fields as $field) {
          $output .= "\$data['" . $field[0] . "']," . "\n";
        }
      }
      else {
        $output = "<p>Available Legacy (Drupal 7) Fields: </p><ul>\n";
        foreach ($fields as $field) {
          $output .= "<li>" . $field[0] . "</li>";
        }
        $output .= "</ul>\n\r";
      }
    }

    return $output;
  }

  /**
   * Start field building process for content type imports.
   */
  public static function GetDrupalFields($nid) {
    $output = [];
    $fields = self::DataSourceQuery("SELECT field_name FROM field_config")->fetch_all();

    if (is_array($fields)) {
      foreach ($fields as $field) {
        $title = self::DataSourceQuery("select title from node where nid = $nid")->fetch_object();
        $output['title'] = $title->title;
        $body = self::DataSourceQuery("select body_value from field_data_body where entity_id = $nid")->fetch_object();
        $output['body'] = $body->body_value;
        $info = $field[0];
        $field_data = self::DataSourceQuery("SELECT * from field_data_$info where entity_id = '$nid'")->fetch_all();
        $output[$info] = $field_data[0];
      }
    }

    return $output;
  }

  /**
   * Get markdown docs, maybe.
   */
  public static function DataSourceQuery($file) {
//    $file = "sites/default/files/markdown/README.md";
    $output = file_get_contents($file);

    var_dump($output);
  }
}
