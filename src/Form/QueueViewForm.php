<?php

namespace Drupal\queue_import\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueWorkerManagerInterface;
use Drupal\Core\Queue\SuspendQueueException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Messenger\MessengerInterface;

/**
 * Class ManuallyRunUpdateTermsForm.
 */
class QueueViewForm extends FormBase {

  /**
   * QueueFactory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * QueueManager.
   *
   * @var \Drupal\Core\Queue\QueueWorkerManagerInterface
   */
  protected $queueManager;

  /**
   * Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * {@inheritdoc}
   */
  public function __construct(QueueFactory $queue, QueueWorkerManagerInterface $queue_manager, MessengerInterface $messenger) {
    $this->queueFactory = $queue;
    $this->queueManager = $queue_manager;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('queue'),
      $container->get('plugin.manager.queue_worker'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'queue_view_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('queue_view_form.settings');

    $numberofitems = ($this->queueFactory->get($config->get('processor'))) ? $this->queueFactory->get($config->get('processor'))->numberOfItems() : 0;

//    $form['fields'] = [
//      '#type' => 'markup',
//      '#markup' => DataSourceConnection::fieldList(TRUE),
//    ];

    $form['queue_select'] = [
      '#type' => 'select',
      '#title' => $this->t('Select Queue'),
      '#options' => self::queueSelect(),
      '#default_value' => $config->get('queue_select'),
      '#weight' => '-99',
      '#empty_option' => $this->t('- Select a queue -'),
      '#ajax' => [
        'callback' => [$this, 'setQueueProcessor'],
        'event' => 'change',
        'progress' => [
          'type' => 'throbber',
          'message' => t('Updating queue processor...'),
        ],
      ],
    ];

    $form['processor'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Queue Processor'),
      '#default_value' => $config->get('processor'),
    ];

    $form['help'] = [
      '#type' => 'markup',
      '#markup' => $this->t('Submitting this form will process the update_terms_processor queue which contains @number items.', ['@number' => $numberofitems]),
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Process queue'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $processor = $form_state->getValue('processor');
    if ($processor != null) {
      $queue = $this->queueFactory->get($processor);
      $queue_worker = $this->queueManager->createInstance($processor);

      while ($item = $queue->claimItem()) {
        try {
          $queue_worker->processItem($item->data);
          $queue->deleteItem($item);
        }
        catch (SuspendQueueException $e) {
          $queue->releaseItem($item);
          break;
        }
        catch (\Exception $e) {
          watchdog_exception('queue_import', $e);
        }
      }
      $this->messenger->addMessage($this->t("Successfully processed queued items."));
    }
    else {
      $this->messenger->addMessage($this->t("No processor setup."));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function queueSelect() {
    $output = ['empty' => 'empty'];
    $connection = \Drupal::database();
    $queues = $connection->query("select distinct name from queue")->fetchAllKeyed();

    if (!empty($queues)) {
      foreach ($queues as $key => $value) {
        $output[$key] = $key;
      }
    }

    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function setQueueProcessor(array $form, FormStateInterface $form_state) {
    $queue = $form_state->getValue('queue_select');
    $config = \Drupal::service('config.factory')->getEditable('queue_view_form.settings');

    $config->set('processor', $queue)->save();

    $response = new AjaxResponse();
    $url = \Drupal::service('path.current')->getPath();
    $response->addCommand(new RedirectCommand('/~owen/devd8/admin/config/development/queue-import/view'));
    return $response;
  }

}
