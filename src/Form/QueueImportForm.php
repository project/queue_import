<?php

namespace Drupal\queue_import\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class QueueImportForm.
 */
class QueueImportForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'queue_import.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'queue_import_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('queue_import.settings');
//    dpm($config->get('db_password'));

    $form['db_host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Host'),
      '#default_value' => $config->get('db_host'),
    ];

    $form['db_username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#default_value' => $config->get('db_username'),
    ];

    $form['db_password'] = [
      '#type' => 'password',
      '#title' => $this->t('Password'),
      '#default_value' => $config->get('db_password'),
      '#size' => 25,
    ];

    $form['db_dbname'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Database'),
      '#default_value' => $config->get('db_dbname'),
    ];

    $form['db_port'] = [
      '#type' => 'number',
      '#title' => $this->t('Port'),
      '#default_value' => ($config->get('db_port')) ? $config->get('db_port') : '3306',
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * Get values via: $config = \Drupal::config('voa_import_content.settings');.
   *
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('queue_import.settings')
      ->set('db_host', $form_state->getValue('db_host'))
      ->set('db_username', $form_state->getValue('db_username'))
      ->set('db_password', $form_state->getValue('db_password'))
      ->set('db_dbname', $form_state->getValue('db_dbname'))
      ->set('db_port', $form_state->getValue('db_port'))
      ->save();
  }

}
