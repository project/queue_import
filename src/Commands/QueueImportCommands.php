<?php

namespace Drupal\queue_import\Commands;

use Drupal\queue_import\Controller\DataSourceConnection;
use Drush\Commands\DrushCommands;
use RecursiveIteratorIterator;
use Symfony\Component\Finder\Iterator\RecursiveDirectoryIterator;
use Drupal\Component\Utility\Html;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 */
class QueueImportCommands extends DrushCommands {

  /**
   * Import Git repo markdown file. Maybe.
   *
   * @command queue-import:md
   * @usage queue-import:md
   *   Imports markdown file from git source.
   */
  public function gitMarkdownContent($url = "https://raw.githubusercontent.com/owenmorrill/snyk-docs/main/test.md") {
    // @todo Add a per-node "synch" button with a link to origin repo file?
    $file = file_get_contents($url);

    $title = 'Test';
    $mid = '9999';

    $data = [
      'title' => $title,
      'body' => Html::escape($file),
      'migration_id' => $mid,
    ];

    $queue = \Drupal::queue('markdown_queue_processor');
    $queue->createQueue();
    $queue->createItem($data);
    \Drupal::logger('queue_import')->info("Markdown file queued: " . $title);
  }

  /**
   * Importing GitBook Markdown content.
   *
   * @command queue-import:gmd
   * @aliases gmd
   * @options menu
   * @usage queue-import:gmd
   *   Imports Drupal content.
   */
  public function gitbookMarkdownContent($options = ['menu' => FALSE]) {
    if ($options['menu']) {
      \Drupal::logger('queue_import')->notice("Creating/updating menu items.");
    }
    $module_handler = \Drupal::service('module_handler');
    $module_path = $module_handler->getModule('queue_import')->getPath();
    $summary = file($module_path . '/mdtest/SUMMARY.md');
    array_filter($summary);
    $gbindex = [];

    foreach ($summary as $key => $summary_line) {
      $parent_alias = '';
      $toplevel = str_starts_with($summary_line, '*');
      if (!str_starts_with($summary_line, '#') && !empty($summary_line)) {
        $clean_line = trim(str_replace('* ', '', $summary_line));
        preg_match('/\[(.*?)\]/s', $clean_line, $matched_title);
        preg_match('/\]\((.*?)$/s', $clean_line, $matched_path);

        if (!empty($matched_title[0]) && !empty($matched_path[0])) {
          $gboutput[0] = substr($matched_path[0], 2, -1);
          $gboutput[1] = substr($matched_title[0], 1, -1);
          $gboutput[2] = $key;

          if ($toplevel === FALSE) {
            $output = str_replace('/README.md', '', $gboutput[0]);
            $output = explode('/', $output);
            array_pop($output);
            foreach ($output as $parent) {
              $parent_alias .= '/'.$parent;
            }
          }
          $gboutput[3] = $parent_alias;
          $gbindex[] = $gboutput;
        }
      }
    }
    \Drupal::logger('queue_import')->notice("Total lines in SUMMARY.md: " . count($summary));
    foreach($gbindex as $file) {
      $parent_alias = '';
      $gbalias = $file[0];
      $gbtitle = $file[1];
      $gbid = $file[2];
      $gbparentid = $file[3];
      if (file_exists($module_path . '/markdown/' . $file[0])) {
        $filepath = $module_path . '/markdown/' . $file[0];
        $gitbook_page = file_get_contents($filepath);

        // if # is not the first character...
        $body = trim(substr($gitbook_page, strlen($gbtitle) + 2));
        $gbalias = substr($gbalias, 0, -3);
        $gbalias = str_replace('/README', '', $gbalias);

        if (!empty($gbtitle)) {
          $data = [
            'title' => $gbtitle,
            'body' => Html::escape($body),
            'gitbook_id' => $gbid,
            'path_alias' => '/' . $gbalias,
            'parent' => $gbparentid ?? NULL,
            'create_nav_items' => $options['menu'],
          ];

          $queue = \Drupal::queue('gitbook_queue_processor');
          $queue->createQueue();
          $queue->createItem($data);
          \Drupal::logger('queue_import')->info("Markdown file queued: " . $gbtitle);
        } else {
          \Drupal::logger('queue_import')->error("First line issue: " . $gbtitle . ' - length of: ' . strlen($gbtitle));
        }
      }
      \Drupal::logger('queue_import')->info("Queue command completed.");
    }
  }

  /**
   * Importing Drupal content.
   *
   * @command queue-import:drupal
   * @aliases d7
   * @options content_type Choose queue worker to use.
   * @options limit Choose how many to import.
   * @usage queue-import:drupal article
   *   Imports Drupal content.
   */
  public function importDrupalContent($content_type = 'node', $limit = 1) {
    $queue = $content_type . '_queue_processor';
    $queue = \Drupal::queue($queue);
    $queue->createQueue();

    $query = "select nid, type from node where type = '$content_type' order by nid desc limit $limit";
    $output = DataSourceConnection::DataSourceQuery($query);

    foreach ($output as $node) {
      $data = DataSourceConnection::GetDrupalFields($node['nid']);
      $data['type'] = $node['type'];
      $data['nid'] = $node['nid'];
      $queue->createItem($data);
      $this->output()->writeln("Queued legacy nid: " . $node['nid']);
    }
  }

  /**
   * Create queue worker field mapping.
   *
   * @command queue-import:field-map
   * @aliases d7fm
   * @options content_type Choose D8/D9 content type to map D7 fields against.
   * @usage queue-import:field-map
   *    Outputs queue worker field mapping for content type.
   */
  public function fieldMap($content_type) {
    $queue_worker_builder = DataSourceConnection::queueWorkerGenerator($content_type);

    $module_handler = \Drupal::service('module_handler');
    $module_path = $module_handler->getModule('queue_import')->getPath();
    $file = "$module_path/src/Plugin/QueueWorker/Map" . ucfirst($content_type) . "QueueProcessor.php";

    $fp = fopen($file, "wb");
    fwrite($fp, $queue_worker_builder);
    fclose($fp);
    $this->output->writeln("Field map: " . print_r($queue_worker_builder));
  }

  /**
   * Test importing articles.
   *
   * @command queue-import:test-article
   * @aliases itest
   * @usage queue-import:test-article
   *   Imports a test article.
   */
  public function importTest() {
    $data = [
      'title' => 'Greatest Import Test Ever',
      'body' => '<p>This will import to the node__body table as the body_value.</p>',
      'main_image' => 'https://thebikeshed.cc/wp-content/uploads/2014/07/Pacific-MC-CX-z.jpg',
      'tags' => ['cx500', 'cafe racer', 'refurbish', 'build'],
      'migration_id' => 1,
    ];

    $queue = \Drupal::queue('node_queue_processor');
    $queue->createQueue();
    $queue->createItem($data);
  }

  /**
   * Deletes or simply counts all nodes of specified content type.
   *
   * Note: Does not work with content_lock module enabled.
   *
   * @command queue-import:count-content
   * @aliases qicount
   * @options content_type Choose between articles, authors, etc.
   * @usage queue-import:count-content
   *   Runs count of all entities of the bundle authors.
   * @usage queue-import:count-content --delete
   *   Returns deletes all author node entities.
   */
  public function deleteNodes($content_type, $options = ['delete' => FALSE]) {
    if ($content_type !== 'image') {
      $result = \Drupal::entityQuery('node')
        ->condition('type', $content_type)
        ->execute();

      if ($options['delete']) {
        // entity_delete_multiple('node', $result);.
        $result = array_slice($result, 0, 5000);

        $storage_handler = \Drupal::entityTypeManager()->getStorage("node");
        $entities = $storage_handler->loadMultiple($result);
        $storage_handler->delete($entities);

        $storage_handler2 = \Drupal::entityTypeManager()->getStorage("path_alias");
        $entities2 = $storage_handler2->loadMultiple($result);
        $storage_handler2->delete($entities2);

        $mids = \Drupal::entityQuery('menu_link_content')
          ->condition('menu_name', 'sidenav')
          ->execute();
        $controller = \Drupal::entityTypeManager()->getStorage('menu_link_content');
        $entities = $controller->loadMultiple($mids);
        $controller->delete($entities);

        $this->output()->writeln('Deleted ' . count($result) . ' ' . $content_type . '(s).');
      }
      else {
        $this->output()->writeln('There are ' . count($result) . ' ' . $content_type . '(s) on the site.');
      }
    }
    else {
      $result = \Drupal::entityQuery('media')
        ->condition('bundle', $content_type)
        ->execute();

      if ($options['delete']) {
        // entity_delete_multiple('node', $result);.
        $result = array_slice($result, 0, 1000);

        $storage_handler = \Drupal::entityTypeManager()->getStorage("media");
        $entities = $storage_handler->loadMultiple($result);
        $storage_handler->delete($entities);

        $this->output()->writeln('Deleted ' . count($result) . ' ' . $content_type . '(s).');
      }
      else {
        $this->output()->writeln('There are ' . count($result) . ' ' . $content_type . '(s) on the site.');
      }
    }
  }

}
