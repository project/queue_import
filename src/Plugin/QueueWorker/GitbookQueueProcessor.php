<?php

namespace Drupal\queue_import\Plugin\QueueWorker;

use Drupal\Component\Utility\Html;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\menu_link_content\Entity\MenuLinkContent;
use Drupal\node\Entity\Node;
use Drupal\path_alias\Entity\PathAlias;

/**
 * Processes Markdown Imports.
 *
 * @QueueWorker(
 *   id = "gitbook_queue_processor",
 *   title = @Translation("Task Worker: Markdown Queue Processor"),
 *   cron = {"time" = 60}
 * )
 */
class GitbookQueueProcessor extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $uuid = NULL;
    $query = \Drupal::entityQuery('path_alias')
      ->condition('alias', $data['path_alias'], 'like');
    $query_path_alias = $query->execute();

    if (!empty($query_path_alias)) {
      $query = \Drupal::entityQuery('node')
        ->condition('type', 'page')
        ->condition('field_migration_id', $data['gitbook_id'], '=');
      $nid = $query->execute();

      $nid = reset($nid);
      $node = Node::load($nid);
    } else {
      $fields = [
        'type' => 'page',
      ];
      $fields = array_filter($fields);
      $node = Node::create($fields);
    }

    $node->title = $data['title'];
    $node->body->value = Html::decodeEntities($data['body']);
    $node->body->format = 'markdown';
    $node->field_migration_id = $data['gitbook_id'];
    $node->set('uid', '1');
    $node->set('moderation_state', "published");
    $node->setPublished();

    $node->save();

    if ($data['path_alias'] === '/README') {
      $data['path_alias'] = '/home';
      $data['gitbook_id'] = '-10000';

      \Drupal::configFactory()
        ->getEditable('system.site')
        ->set('page.front', '/node/'.$node->id())
        ->save();
    }

    if (empty($query_path_alias)) {
      PathAlias::create([
        'path' => "/node/" . $node->id(),
        'alias' => $data['path_alias'],
        'langcode' => 'en',
      ])->save();
    } else {
      // Update alias.
      $alias = PathAlias::load(reset($query_path_alias));
      $alias->setPath("/node/" . $node->id());
      $alias->alias = $data['path_alias'];
      $alias->save();
    }

    // Alright, let's find us a parent and create a menu item.
    if ($data['create_nav_items'] && !is_null($data['parent'])) {
      $queryalias1 = \Drupal::entityQuery('path_alias')
        ->condition('alias', $data['parent'], '=');
      $alias1 = $queryalias1->execute();
      if (!empty($alias1)) {
        $alias2 = PathAlias::load(reset($alias1));
        if (is_object($alias2)) {
          $nodepath = $alias2->get('path')->getValue();
          $nodepath = $nodepath[0]['value'];
        }
      }
      if (!empty($nodepath)) {
        $nodepath2 = substr($nodepath, 1);
        $query3 = \Drupal::entityQuery('menu_link_content')
          ->condition('link.uri', 'entity:' . $nodepath2);
        $result = $query3->execute();
      }
      if (!empty($query3)) {
        $alias3 = MenuLinkContent::load(reset($result));
        if (is_object($alias3)) {
          $uuid = $alias3->get('uuid')->getValue();
          $uuid = 'menu_link_content:' . $uuid[0]['value'];
        }
      }

      // Create a menu item if none exist.
      $query = \Drupal::entityQuery('menu_link_content')
        ->condition('link.uri', 'entity:node/' . $node->id());
      $result = $query->execute();

      if (is_array($result) && empty($result)) {
        MenuLinkContent::create([
          'title' => $node->title,
          'link' => ['uri' => 'entity:node/' . $node->id()],
          'menu_name' => 'sidenav',
          'parent' => $uuid ?? NULL,
          'expanded' => TRUE,
          'weight' => $data['gitbook_id'],
        ])->save();
      } else {
        $menuitem = MenuLinkContent::load(reset($result));
        $menuitem->title = $node->title;
        $menuitem->link = ['uri' => 'entity:node/'.$node->id()];
        $menuitem->parent = $uuid ?? NULL;
        $menuitem->weight = $data['gitbook_id'];
        $menuitem->save();
      }
    }
  }

}
