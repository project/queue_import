<?php

namespace Drupal\queue_import\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\node\Entity\Node;
use Drupal\queue_import\Controller\QueueImportController;

/**
 * Processes Article Imports.
 *
 * @QueueWorker(
 *   id = "node_queue_processor",
 *   title = @Translation("Task Worker: Node Queue Processor"),
 *   cron = {"time" = 60}
 * )
 */
class NodeQueueProcessor extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $terms = $data['tags'];
    $output_terms = [];
    foreach ($terms as $term) {
      $output_term = QueueImportController::setToTerm($term);
      $output_terms[] = $output_term;
    }
    $fields = [
      'type' => 'article',
    ];

    $image_id = QueueImportController::generateMediaEntity($data['main_image']);

    $fields = array_filter($fields);
    $node = Node::create($fields);
    $node->title = $data['title'];
    $node->body->value = $data['body'];
    $node->body->format = 'full_html';
    $node->field_image = $image_id;
    $node->field_tags = $output_terms;
    $node->field_migration_id = $data['migration_id'];

    $node->save();
  }

}
