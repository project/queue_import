<?php

namespace Drupal\queue_import\Plugin\QueueWorker;

use Drupal\Component\Utility\Html;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\menu_link_content\Entity\MenuLinkContent;
use Drupal\node\Entity\Node;
use Drupal\path_alias\Entity\PathAlias;
use Drupal\queue_import\Controller\QueueImportController;

/**
 * Processes Markdown Imports.
 *
 * @QueueWorker(
 *   id = "markdown_queue_processor",
 *   title = @Translation("Task Worker: Markdown Queue Processor"),
 *   cron = {"time" = 60}
 * )
 */
class MarkdownQueueProcessor extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $fields = [
      'type' => 'product_updates',
    ];
    $fields = array_filter($fields);
    $node = Node::create($fields);

    $node->title = $data['title'];
    $node->body->value = Html::decodeEntities($data['body']);
    $node->body->format = 'markdown';
    $node->field_migration_id = $data['migration_id'];
    $node->set('uid', '1');

    $node->save();

  }

}
